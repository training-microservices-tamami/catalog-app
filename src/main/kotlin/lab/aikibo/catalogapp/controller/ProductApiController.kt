package lab.aikibo.catalogapp.controller

import lab.aikibo.catalogapp.entity.Product
import lab.aikibo.catalogapp.repo.ProductRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController @RequestMapping("/api/product")
class ProductApiController {

    @Autowired
    lateinit var productRepo: ProductRepo

    @GetMapping("/")
    fun findProducts(page: Pageable): Page<Product> = productRepo.findAll(page)

}