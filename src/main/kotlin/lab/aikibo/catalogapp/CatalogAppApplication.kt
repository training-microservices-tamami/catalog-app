package lab.aikibo.catalogapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CatalogAppApplication

fun main(args: Array<String>) {
	runApplication<CatalogAppApplication>(*args)
}
