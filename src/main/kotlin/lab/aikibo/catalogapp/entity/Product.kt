package lab.aikibo.catalogapp.entity

import lombok.NonNull
import org.hibernate.annotations.GenericGenerator
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
data class Product(
        @Id @GeneratedValue(generator = "uuid") @GenericGenerator(name = "uuid", strategy = "uuid2")
        var id: String,

        @NotNull @NotEmpty @Size(max = 100)
        var code: String,

        @NotNull @NotEmpty
        var name: String
)