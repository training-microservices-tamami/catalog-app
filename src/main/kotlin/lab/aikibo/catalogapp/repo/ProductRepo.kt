package lab.aikibo.catalogapp.repo

import lab.aikibo.catalogapp.entity.Product
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepo: PagingAndSortingRepository<Product, String> {
}